import json

with open('data/targets.json', 'r') as fd:
    oldtargets = json.load(fd)

with open('data/targets2.json', 'r') as fd:
    newtargets = json.load(fd)

oldR = {}

for recipe in oldtargets['recipes']:
    oldR[recipe['title'].lower()] = True

newrecipes = []
newjson = {"target_total": 0}

for recipe in newtargets['recipes']:
    if recipe['title'].lower() not in oldR:
        newrecipes.append(recipe)
        newjson['target_total'] += 1

newjson['recipes'] = newrecipes

with open('data/newtargets.json', 'w') as fd:
    json.dump(newjson, fd, indent=4)


