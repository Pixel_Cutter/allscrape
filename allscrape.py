from bs4 import BeautifulSoup as bs
import requests
import json
from time import sleep
import shutil
from random import randint, sample

baseURL = "https://www.allrecipes.com/search/results/?search="
dishDict = { "target_count": 0 }
dishDict['recipes'] = []
scrapedDict = {}

def dumpDict() -> None:
    with open("data/targets2.json", "w") as fd:
        json.dump(dishDict, fd, indent = 4)

def addTarget(name: str, link: str, summary: str, imgLink: str) -> None:

    # if dish already scraped, do nothing
    if name in scrapedDict:
        return

    dishDict['recipes'].append({
            "title": name.title(),
            "link": link,
            "summary": summary,
            "imgLink": imgLink
        })

    scrapedDict[name] = True;
        
    dishDict['target_count'] += 1

def parseCards(recipeCards: list) -> None:
    for card in recipeCards:
        aTag = card.find('a')
        link = aTag.get('href')
        name = aTag.get('title').lower()
        summary = card.select('.card__summary')[0].text.strip()
        imgLink = card.find('img').get('src')
        
        addTarget(name, link, summary, imgLink)
    
def initFoodList(foods) -> list:
    foodList = []
    with open(foods, 'r') as fd:
        for row in fd:
            foodList.append(row.split(',')[0].lower())

    return foodList

def getQuery(foodList: list) -> str:
    query = ' '.join(sample(foodList, randint(1,4)))

    return query

def main():
    foodList = initFoodList("data/generic_foods.csv")
    visitedDict = {}

    while dishDict["target_count"] < 10000:
        query = getQuery(foodList)
        sortedQ = ''.join(sorted(query))
    
        # if query was already used (even if reversed), get new query
        if sortedQ in visitedDict:
            continue

        targetURL = baseURL + query
        
        try:
            page = requests.get(targetURL)
            soup = bs(page.text, 'html.parser')
            recipeCards = soup.select('.card__facetedSearchResult')
            parseCards(recipeCards)
        except:
            continue

        visitedDict[sortedQ] = True
        sleep(1)
        
        print(f"Query, '{query}', Complete\nTarget Count: {dishDict['target_count']}")

    dumpDict()


main()

