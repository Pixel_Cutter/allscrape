from bs4 import BeautifulSoup as bs
import requests
import json
from time import sleep

with open('data/newtargets.json', 'r') as fd:
    db = json.load(fd)

def dumpDict(db: dict) -> None:
    with open("data/newrecipes.json", "w") as fd:
        json.dump(db, fd, indent = 4)

def getURL(url, retries = 3):
    try:
        page = requests.get(url)
        return page
    except:
        if retries < 0:
            dumpDict(db)
            exit(0)

        sleep(5)
        getURL(url, (retries - 1))

def getNutrition(soup) -> dict:
    nutritionInfo = {}
    nutritionInfo['servings'] = soup.select('.recipe-adjust-servings__size-quantity')[0].text
    nutritionInfo['calPerServ'] = soup.select('.nutrition-top')[0].text.split(':')[-1].strip()
    nutritionInfo['nutrients'] = []
    
    allNutrients = soup.select('.nutrition-row')

    for nutrient in allNutrients:
        nutrientName = nutrient.text.split(':')[0].strip()
        nutrientQuantity = nutrient.span.span.get('aria-label')
      
      # strSplit = nutrientStr.split('.')
        
       # try:
       #     nutrientQuantity = float(strSplit[0] + '.' + strSplit[1][:1])
       #     nutrientUnit = strSplit[1][1:]
       # except:
       #     nutrientQuantity = "None"
       #     nutrientUnit = "None"

        nutritionInfo['nutrients'].append({
            "name": nutrientName,
            "quantity": nutrientQuantity
        })

    return nutritionInfo

def getSteps(soup) -> list:
    steps = soup.select('.instructions-section-item')
    stepList = []

    for step in steps:
        stepList.append(step.p.text)

    return stepList


def getIngredients(soup) -> list:
    ingredients = soup.select('.ingredients-item')
    ingredientList = []

    for item in ingredients:
        ingredient = item.input.get('data-ingredient')
        quantity = item.input.get('data-quantity')
        unit = item.input.get('data-unit')
        combined = f"{quantity} {unit} {ingredient}"

        ingredientList.append({
            "ingredient": ingredient,
            "quantity": quantity,
            "unit": unit,
            "value": combined
        })

    return ingredientList

def main():
 
    completed = []
    db['total_recipes'] = 0

    for recipe in db['recipes']:
        targetURL = recipe['link']
        page = getURL(targetURL)
        soup = bs(page.text, 'html.parser')
    
        try:
            recipe['ingredients'] = getIngredients(soup)
            recipe['steps'] = getSteps(soup)
            recipe['nutritionInfo'] = getNutrition(soup)
        except:
            continue
        
        completed.append(recipe)
        db['total_recipes'] += 1

        print(f"Recipe, {recipe['title']}, Complete\nRecipe Done Count: {db['total_recipes']}")
        
        sleep(1)

    db['recipes'] = completed
    
    dumpDict(db)


main()

