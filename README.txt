Files contained in repo:

	allscrape.py			A program that partially scrapes recipes found from random search queries to allrecipes.com.
							The recipes scraped will contain a recipe name, link to recipe, link to recipe image,
							and a brief recipe summary. All scraped recipe info will be placed in data/targets.json.

	singlescrape.py			A program that completes the scraping of a recipe. Singlescrape will loop through the
							targets.json file and complete each recipe with a list of ingredients, a list of steps,
							and a full nutrition breakdown. Completed recipes will be placed in data/recipes.json.

	data/generic_foods.csv	Contains ~880 generic names of food items (chicken, adobo, etc..).
							Used by allscrape.py to make random queries

	data/recipes.json		All scraped recipes found with allscrape.py.
							Used/produced by singlescrape.py

	data/targets.json		A list of recipes found by allscrape.py.
							Used by singlescrape.py

	requirements.txt		List of dependencies needed for allscrape/singlescrape.
							Activate your virtual environment and then run  the command
							'pip install -r requirements.txt' to quickly install needed
							dependencies.
