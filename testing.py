import requests
import json
from bs4 import BeautifulSoup as bs

page = requests.get("https://www.allrecipes.com/element-api/content-proxy/faceted-searches-load-more?search=duck")
data = page.json()
soup = bs(data['html'], 'html.parser')

print(soup.prettify())
